//
// Willem Hendriks - trying to implement Go rules
//

function zero_board(){
	return [new Array(18).fill(0), new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0),new Array(18).fill(0)];
}

function tf_to_js(tf_board){
	planes = 18;
	board_size = 19;

	js_board = [zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board(), zero_board()];

	for (x = 0; x < board_size; x++){
		for (y = 0; y < board_size; y++){
			for (p = 0; p < planes; p++){
				js_board[x][y][p] = tf_board[p+18*(19*x)+18*y];
			}
		}
	}
	return js_board;
}

//js_board = tf_to_js(tf_input_transpose.dataSync());

var checked = [];

function has_liberties_helper( js_board, i,j, checked, my_plane=0, enemy_plane=8 ){
	//
	// - We assume the player to move is my_plane, want to check liberties, at point (i,j)
	// - Enemy player is on enemy_plane
	// - Point (i,j) can be empty, or contain a stone. When already a stone, it is helpful 
	// for chechking enemy groups being dead after a stone is placed.
	//
    
    //
    // Check, if the point is on the board
    //
    if( i > 18 || i < 0 || j > 18 || j < 0 ){ // IS_ON_BOARD
        console.log( 'not-on-board' + i + ',' + j );
        return false;
    }
	//
	// If already have checked for liberties, skip this point
	//
    if( checked.includes( i + "-" + j ) ){
        console.log( "already checked " + i + "-" + j );
        return false;
    }

	//
	// If this is a enemy stone, it can't be helpful to check your liberties
	//
    if( js_board[i][j][enemy_plane] == 1 ){ // # IS_ENEMY_STONE
        return false;
    }
    
	//
	// Check all neighbors for being empty spots
	//
    if( js_board[Math.abs(i-1)][j][my_plane] == 0 && !checked.includes( Math.abs(i-1)+"-"+j  ) && js_board[Math.abs(i-1)][j][enemy_plane] == 0 ){
        //console.log( "found liberty at " + Math.abs(i-1)+'-'+j+ ", for stone at" + i+'-'+j);
        return true;
    }
    if( js_board[i][Math.abs(18 - Math.abs(17 - j))][my_plane] == 0 && !checked.includes( i + "-" + Math.abs(18 - Math.abs(17 - j)) ) && js_board[i][Math.abs(18 - Math.abs(17 - j))][enemy_plane] == 0){
        //console.log( "found liberty at {},{}, for stone at {},{}".format(i,Math.abs(18 - Math.abs(17 - j)), i,j))
        return true;
    }
    if( js_board[i][Math.abs(j-1)][my_plane] == 0 && !checked.includes( i +"-" + Math.abs(j-1) ) && js_board[i][Math.abs(j-1)][enemy_plane] == 0){
        //console.log( "found liberty at {},{}, for stone at {},{}".format(i,Math.abs(j-1), i,j))
        return true;
    }
    if( js_board[Math.abs(18 - Math.abs(17 - i))][j][my_plane] == 0 &&  !checked.includes( Math.abs(18 - Math.abs(17 - i)) + "-" + j) && js_board[Math.abs(18 - Math.abs(17 - i))][j][enemy_plane] == 0){
        //console.log( "found liberty at {},{}, for stone at {},{}".format(Math.abs(18 - Math.abs(17 - i)),j, i,j))
        return true;
    }

	//
	// Add points to points that are checked for neighbors, but dont have direct liberties
	//
    checked.push( i+"-"+j );
    
	//
	// Expand search for liberties by checking the friendly neigbor stones for liberties.
	// 
	// It is OK if points are checked outside board, of of enemy stones, because those searched get killed 
	//

    //# FOR ALL FRIENLY NEIGHBOR STONES
    return has_liberties_helper(js_board, i-1,j, checked, my_plane=my_plane, enemy_plane=enemy_plane) || has_liberties_helper(js_board, i+1,j, checked, my_plane=my_plane, enemy_plane=enemy_plane) || has_liberties_helper(js_board, i,j-1, checked, my_plane=my_plane, enemy_plane=enemy_plane) || has_liberties_helper(js_board, i,j+1, checked, my_plane=my_plane, enemy_plane=enemy_plane)

}

//js_board = tf_to_js(tf_input_transpose.dataSync());
//has_liberties_helper( js_board, 0,0, checked, my_plane=0, enemy_plane=8 )

function has_liberties(js_board, i,j, my_plane=0, enemy_plane=8){
    return has_liberties_helper( js_board, i,j, [], my_plane=my_plane, enemy_plane=enemy_plane)
}


// Remove stone(s) from board
function remove_stones( js_board, i, j, plane){
    LEFT = [Math.abs(i-1),j];
    RIGHT = [Math.abs(18 - Math.abs(17 - i)),j];
    UP = [i,Math.abs(j-1)];
    DOWN = [i,Math.abs(18 - Math.abs(17 - j))];
    
    var neighbors =  [LEFT,RIGHT,UP,DOWN];

    if( js_board[i][j][plane] == 1){
        js_board[i][j][plane] = 0
        for( var n = 0; n < neighbors.length; n++ ){
            k = neighbors[n][0];
        	l = neighbors[n][1];
            if( js_board[k][l][plane] == 1){
                remove_stones( js_board, k, l, plane)
            }
        }
     }

    return true;
}

function place_stone( js_board, i, j){
    if( !js_board[i][j][0] == 0 || !js_board[i][j][8] == 0){
        console.log( "Already stone at" +i +','+ j );
        return false;
    }
    
    var PLACED = false;

    if( has_liberties(js_board, i, j)){
    	//console.log("has_liberty");
        js_board[i][j][0] = 1;
        PLACED = true;
    }
    
    LEFT = [Math.abs(i-1),j];
    RIGHT = [Math.abs(18 - Math.abs(17 - i)),j];
    UP = [i,Math.abs(j-1)];
    DOWN = [i,Math.abs(18 - Math.abs(17 - j))];

    neighbors = [LEFT,RIGHT,UP,DOWN];
    //console.log(NEIGHBORS);

    for( var n = 0; n < neighbors.length; n++ ){
        k = neighbors[n][0];
    	l = neighbors[n][1];
      	//console.log( "" + k +'-' + l );//
        if( js_board[k][l][8] == 1 && !has_liberties_helper(js_board,k,l, checked=[ i+"-"+j ], my_plane=8, enemy_plane=0) ){
            //print("no liberties for {},{} - stone have to be removed".format(k,l))
            remove_stones(js_board,k,l,8)
            js_board[i][j][0] = 1;
            PLACED = true;
        }
    }
            
    if( !PLACED){
        console.log( "Couldn't place stone at " + i +"," + j);
    }
        
    return PLACED;
}

function transpose(a) {
    return Object.keys(a[0]).map(function(c) {
        return a.map(function(r) { return r[c]; });
    });
}

function transpose_board(js_board){
	var tjs = [transpose( js_board[0] ), transpose( js_board[1] ), transpose( js_board[2] ), transpose( js_board[3] ), transpose( js_board[4] ), transpose( js_board[5] ), transpose( js_board[6] ), transpose( js_board[7] ), transpose( js_board[8] ), transpose( js_board[9] ), transpose( js_board[10] ), transpose( js_board[11] ), transpose( js_board[12] ), transpose( js_board[13] ), transpose( js_board[14] ), transpose( js_board[15] ), transpose( js_board[16] ), transpose( js_board[17] ), transpose( js_board[18] )];
	return transpose(tjs);
}

function de_transpose_board(js_board_t){
	var js_board_tt = transpose(js_board_t);
	var ttjs = [transpose( js_board_tt[0] ), transpose( js_board_tt[1] ), transpose( js_board_tt[2] ), transpose( js_board_tt[3] ), transpose( js_board_tt[4] ), transpose( js_board_tt[5] ), transpose( js_board_tt[6] ), transpose( js_board_tt[7] ), transpose( js_board_tt[8] ), transpose( js_board_tt[9] ), transpose( js_board_tt[10] ), transpose( js_board_tt[11] ), transpose( js_board_tt[12] ), transpose( js_board_tt[13] ), transpose( js_board_tt[14] ), transpose( js_board_tt[15] ), transpose( js_board_tt[16] ), transpose( js_board_tt[17] ), transpose( js_board_tt[18] ) ];
	return transpose(ttjs);
}


function switch_player(js_board){
	var js_board_t = transpose_board(js_board);
	var switched_t = [ js_board_t[8], js_board_t[9], js_board_t[10], js_board_t[11], js_board_t[12], js_board_t[13], js_board_t[14], js_board_t[15], js_board_t[0], js_board_t[1], js_board_t[2], js_board_t[3], js_board_t[4], js_board_t[5], js_board_t[6], js_board_t[7], js_board_t[17], js_board_t[16] ];
	window.js_board = de_transpose_board(switched_t);
}

