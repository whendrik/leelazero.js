# LeelaZero.js

- [x] Export Model to Keras
- [X] Export Keras to Tensorflow.js
- [X] Board Encoder to 18 Planes, for [jgoboard](http://jgoboard.com/)
- [X] Live 1 playout version, i.e. let bot play move with highest Policy value
- [ ] Basic MC
- [ ] Other MC
- [ ] Full MCTS, similar to LeelaZero
- [ ] ...

## .JS Go Rules

With exception of Ko-rule, the Go rules are implemented in [flask/static/leelazero.js](flask/static/leelazero.js)

## Online Demo

Play against the `10x128` single playout network. Wait for the network to load, on slow connections this might take a while.

https://www.alphago-games.com/static/jgoboard/leelazero_test.html

Check `flask/static/jgoboard/leelazero_test.html` for code.

## Build a LeelaZero.js yourself

### LeelaZero to Keras

Use [LeelaZero to Keras](https://gitlab.com/whendrik/leelazerotokeras/) to export a LeelaZero network to Keras. The output will be a file like `leelazero_1x8.h5`.

Make sure the Model is exported with nightly 2.0, `pip install -U tf-nightly-2.0-preview` (https://github.com/tensorflow/tfjs/issues/1255).

### Keras to Tensorflow.js 

With `leelazero_1x8.h5` being a output of LeelaZero to Keras Conversion

```
pip install tensorflowjs

tensorflowjs_converter --input_format keras leelazero_1x8.h5 leelazero_js_output
```

## Call in Javascript

Make sure to refresh the browsers cache when testing new models, Chrome is very persistent with JavaScript Cache.

```
import * as tf from '@tensorflow/tfjs';

const model = await tf.loadLayersModel('http://127.0.0.1:5000/static/leelazero_js_output/model.json');

empty_board = tf.zeros([1,19,19,18])

preds = model.predict(empty_board)

policy_out = preds[0]
value_out = preds[1]

policy_out.print()
policy_out.array()

```

## Future Ideas

- Create a heatmap with a modified version of `drawmark()` in JGO. This will make it possible to overlap props of policy or MC resuls